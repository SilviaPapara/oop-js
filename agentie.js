// Clasa Imobil
function Imobil(proprietar, agent, numeStrada, pretVanzare, pozaProprietare) {
    this.proprietar = proprietar;
    this.agent = agent;
    this.numeStrada = numeStrada;
    this.pretVanzare = pretVanzare;
    this.pozaProprietare = pozaProprietare;
    
    // Asignarea agent la imobil
    this.addAgent = (agent) => {
        this.agent = agent;
    }
    
    // Asignare proprietar la imobil
    this.addProprietar = (proprietar) => {
        this.proprietar = proprietar;
    }
    
    // Asignare client Potential la imobil
    this.addClientPot = (clientPot, valOferta) => {
        this.clientPot = clientPot;
        this.valOferta = valOferta;
    }
    
    // Eliminare client Potential la imobil
    this.removeClientPot = () => {
        this.clientPot = undefined;
        this.valOferta = undefined;
    }

    //Calculare metoda calcul oferta - pret
    this.calcul = () => Math.floor(100 - ((this.valOferta * 100) / this.pretVanzare));
}

// Clasa Locuinta derivata din clasa Imobil
function Locuinta(proprietar, agent, numeStrada, pretVanzare, pozaProprietare, etaj, nrCamere) {
    Imobil.call(this, proprietar, agent, numeStrada, pretVanzare, pozaProprietare);
    
    this.etaj = etaj;
    this.nrCamere = nrCamere;
}

// Clasa Teren derivata din clasa Imobil 
function Teren(proprietar, agent, numeStrada, pretVanzare, pozaProprietare, dimensiune) {
    Imobil.call(this, proprietar, agent, numeStrada, pretVanzare, pozaProprietare)
    
    this.dimensiune = dimensiune;
}

// Clasa Apartament derivata din clasa Locuinta
function Apartament(proprietar, agent, numeStrada, pretVanzare, pozaProprietare, etaj, nrCamere) {
    Locuinta.call(this, proprietar, agent, numeStrada, pretVanzare, pozaProprietare, etaj, nrCamere)  
}

// Clasa Casa derivata din clasa Locuinta
function Casa(proprietar, agent, numeStrada, pretVanzare, pozaProprietare, etaj, nrCamere, curte) {
    Locuinta.call(this, proprietar, agent, numeStrada, pretVanzare, pozaProprietare, etaj, nrCamere)
    
    this.curte = curte;
}

//Clasa Persoana
function Persoana(nume, varsta, mail, telefon){
    this.nume = nume;
    this.varsta = varsta;
    this.mail = mail;
    this.telefon = telefon;
}

//Clasa Client derivata din clasa Persoana
function Client(nume, varsta, mail, telefon, buget) {
    Persoana.call(this, nume, varsta, mail, telefon)
    this.buget = buget;
}

//Clasa Proprietar derivata din clasa Persoana
function Proprietar(nume, varsta, mail, telefon) {
    Persoana.call(this, nume, varsta, mail, telefon)
}

//Clasa Agent derivata din clasa Persoana
function Agent(nume, varsta, mail, telefon) {
    Persoana.call(this, nume, varsta, mail, telefon)
}

// Clasa Poza
function Poza(URL,nrOrdine,descriere) {
    this.URL = URL;
    this.nrOrdine = nrOrdine;
    this.descriere = descriere;
}

// Instantarea imobilelor
proprietar1 = new Proprietar('Vasile', 20, 'vasile@yahoo.com', '0753918398');

proprietar2 = new Proprietar('Costel', 50, 'costel@yahoo.com', '0754118398');

proprietar3 = new Proprietar('Alin', 70, 'alin@yahoo.com', '0233228899');

agent1 = new Agent ('Minut', 40, 'agent1@yahoo.com','0756565656');

agent2 = new Agent ('Paul', 34, 'agent2@yahoo.com','0244555555');

client1 = new Client('Popescu', 27, 'popescu@gmail.com', '077577777', 1000);

poza1 = new Poza('www.poza1.com', 1, 'descriere1');
imobil1 = new Casa(proprietar1, agent1, 'Maratei', 1500, poza1, 1, 5, true);

poza2 = new Poza('www.poza2.com', 2, 'descriere2');
imobil2 = new Apartament(proprietar2, agent1, 'Pacurari', 2500, poza2, 8, 3);  

poza3 = new Poza('www.poza3.com', 3, 'descriere3');
imobil3 = new Teren(proprietar1, agent1, 'LouisP', 2000, poza3, 400);  

poza4 = new Poza('www.poza4.com', 4, 'descriere4');
imobil4 = new Teren(proprietar3, agent2, 'Garii', 5000, poza4, 60);

imobil1.addClientPot(client1, 1450);
imobil3.addClientPot(client1, 1980);
imobil4.addClientPot(client1, 1000);

listaImobile = [imobil1, imobil2, imobil3, imobil4];

listaImobile.forEach(imobil => {
    if(imobil.clientPot === undefined)
        console.log(imobil);
});

listaImobile.forEach(imobil => {
    if(imobil.calcul() < 10) {
            imobil.valOferta += (15 * 100) / imobil.valOferta;
            //imobil.valOferta = null; - eliberarea memoriei
            delete imobil.valOferta;
    }
});

console.log('Toate imobilele:');

listaImobile.forEach(imobil => {
    console.log(imobil);
});